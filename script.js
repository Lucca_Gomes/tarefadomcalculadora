let notaInput = document.querySelector(".notaInput");
let botaoNota = document.querySelector(".adicionarBotao");
let botaoMedia = document.querySelector(".botaoMedia");
let campoNotas = document.querySelector(".campoNotas");
let resultado = document.querySelector(".resultado");
let listaNotas = [];
let contador = 0;

botaoNota.addEventListener("click", () => {
    nota = notaInput.value;
    if(nota == ""){
        alert("Por favor, insira uma nota!");
    }else{
        padronizarNota();
        if(0 <= nota && nota<= 10){
            listaNotas.push(nota);
            contador++;
            let textoNota = ("a nota " + contador + " foi: " + nota);
            notaInput.value = "";
            let elementoNota = document.createElement("p");
            elementoNota.innerHTML = textoNota;
            campoNotas.appendChild(elementoNota);
        }
        else if(0 > nota || 10 < nota) {
            alert("A nota digitada é inválida, por favor, insira uma nota válida.")
            notaInput.value = "";
        }else {
            alert("A nota digitada é inválida, por favor, insira uma nota válida.")
            notaInput.value = "";
        }
    }
});



botaoMedia.addEventListener("click", () => {
    let soma = 0;
    for (i = 0; i < listaNotas.length; i++){
        let notasn = parseFloat(listaNotas[i]);
        soma += notasn;
    }
    let media = soma / listaNotas.length;
    let mediafix2 = media.toFixed(2);
    let mediafinal = document.createElement("span");
    mediafinal.innerHTML = `<span>${mediafix2}</span>`
    resultado.innerText = "";
    resultado.appendChild(mediafinal);
    listaNotas = [];
    contador = 0;
    campoNotas.innerText = "";
})


function padronizarNota() {
        nota = nota.toString();
        nota = parseFloat(nota.replace(",", "."));
}